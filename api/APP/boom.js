const express = require('express');
const config = require('../config');
const path = require('path');
const Boom = require("../models/Boom");
const { nanoid } = require('nanoid');
const multer = require('multer');
const auth = require("../middleware/auth");
const User = require("../models/User");


const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname))
  }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
  try {
    const post = await Boom.find().populate('user');
    return res.send(post);
  } catch (e) {
    next(e);
  }
});

router.get('/:id', async (req, res, next) => {
  try {
    const post = await Boom.findById(req.params.id);
    if (!post) {
      return res.status(404).send({message: 'Not found'});
    }
    return res.send(post);
  } catch (e) {
    next(e);
  }
});


router.post('/', upload.single('image'), async (req, res, next) => {
  try {
    if (!req.body.title) {
      return res.status(400).send({message: 'title are required'});
    }
    const boomData = {
      title: req.body.title,
      description: req.body.description,
      price: req.body.price,
      category: req.body.category,
    };

    const token = req.get('Authorization');
    if (!token) return res.status(401).send({error: 'No token'});
    const user = User.findOne({token});
    if (!user){
      return res.status(401).send({error: 'This token incorrect!' })
    }
    boomData.user = user._id;

    if (req.file) {
      boomData.image = req.file.filename;
    }

    const boomBase = new Boom(boomData);

    await boomBase.save();

    return res.send({message: 'Created new boom', id: boomBase._id});
  } catch (e) {
    next(e);
  }
});

module.exports = router;